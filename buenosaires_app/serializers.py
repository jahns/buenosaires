from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Solicitud, UserProducto, ProductoDespacho

# Create your serializers here.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = (
            'url',
            'id',
            'first_name',
            'last_name',
            'email',
            'username',
            'password'
        )

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user
    
    def update(self, instance, validated_data):
        instance.first_name = validated_data['first_name']
        instance.last_name = validated_data['last_name']
        instance.email = validated_data['email']
        instance.username = validated_data['username']
        instance.set_password(validated_data['password'])
        instance.save()
        return instance
        
    def destroy(self, request, *args, **kwargs):
       user = request.user
       return super(UserSerializer, self).destroy(request, *args, **kwargs)

class UserStaffSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = (
            'url',
            'id',
            'first_name',
            'last_name',
            'email',
            'username',
            'password'
        )

    def create(self, validated_data):
        user = super(UserStaffSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.is_staff = True
        user.save()
        return user
    
    def update(self, instance, validated_data):
        instance.first_name = validated_data['first_name']
        instance.last_name = validated_data['last_name']
        instance.email = validated_data['email']
        instance.username = validated_data['username']
        instance.set_password(validated_data['password'])
        instance.save()
        return instance
        
    def destroy(self, request, *args, **kwargs):
       user = request.user
       return super(UserStaffSerializer, self).destroy(request, *args, **kwargs)

class SolicitudSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Solicitud
        fields = (
            'url',
            'id',
            'fecha',
            'hora',
            'descripcion',
            'tipo',
            'estado',
            'user'
        )

class UserProductoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserProducto
        fields = (
            'url',
            'id',
            'user',
            'producto'
        )

class ProductoDespachoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProductoDespacho
        fields = (
            'url',
            'id',
            'user',
            'producto',
            'direccion',
            'estado',
            'fecha_pedido',
            'hora_pedido',
            'fecha_despacho',
            'hora_despacho',
            'fecha_entrega',
            'hora_entrega'
        )