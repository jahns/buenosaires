from django.apps import AppConfig


class BuenosairesAppConfig(AppConfig):
    name = 'buenosaires_app'
