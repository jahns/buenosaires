from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Solicitud(models.Model):
    fecha = models.CharField(max_length = 10)
    hora = models.CharField(max_length = 5, default = "00:00")
    descripcion = models.CharField(max_length = 250)
    tipos_solicitud = {
        ('R', 'Reparación'),
        ('M', 'Mantención'),
    }
    tipo = models.CharField(max_length = 50, choices = tipos_solicitud)
    estados_solicitud = {
        ('P', 'Por confirmar'),
        ('C', 'Confirmado'),
    }
    estado = models.CharField(max_length = 50, choices = estados_solicitud, default = "P")
    user = models.ForeignKey(User, on_delete = models.CASCADE)

    def __str__(self):
        return "solicitud"

class UserProducto(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    producto = models.CharField(max_length = 200)

    def __str__(self):
        return "userProducto"

class ProductoDespacho(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    producto = models.CharField(max_length = 200)
    direccion = models.CharField(max_length = 300)
    estados_despacho = {
        ('B', 'Bodega'),
        ('D', 'Despachado'),
        ('E', 'Entregado')
    }
    estado = models.CharField(max_length = 50, choices = estados_despacho, default = "B")
    fecha_pedido = models.CharField(max_length = 10, null = True)
    hora_pedido = models.CharField(max_length = 5, null = True, default = "00:00")
    fecha_despacho = models.CharField(max_length = 10, null = True)
    hora_despacho = models.CharField(max_length = 5, null = True, default = "00:00")
    fecha_entrega = models.CharField(max_length = 10, null = True)
    hora_entrega = models.CharField(max_length = 5, null = True, default = "00:00")

    def __str__(self):
        return "productoDespacho"