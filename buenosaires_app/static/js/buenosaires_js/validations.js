requiredMessage = "Este campo es requerido!"
maxlengthMessage = "Largo maximo "

//añadir regla para validar select
$.validator.addMethod("requiredSelect", function(value, element, arg){
    return arg !== value
})

//validar formulario de registro
var validarFormRegistro = $("#ba-form-registro").submit(function(e){
    e.preventDefault()
}).validate({
    rules: {
        /*rut: {
            required: true,
            maxlength: 10
        },*/
        nombre: {
            required: true,
            maxlength: 150,
        },
        paterno: {
            required: true,
            maxlength: 150,
        },
        /*materno: {
            required: true,
            maxlength: 150,
        },
        nacimiento: {
            required: true,
            maxlength: 10,
        },*/
        correo: {
            required: true,
            maxlength: 150,
            email: true
        },
        /*telefono: {
            required: true,
            maxlength: 9,
        },*/
        regUsername: {
            required: true,
            maxlength: 100,
        },
        regPassword: {
            required: true,
            maxlength: 100,
        },
        regRePassword: {
            required: true,
            maxlength: 100,
            equalTo: "#regPassword"
        }
    },
    messages: {
        /*rut: {
            required: requiredMessage,
            maxlength: maxlengthMessage+10
        },*/
        nombre: {
            required: requiredMessage,
            maxlength: maxlengthMessage+150
        },
        paterno: {
            required: requiredMessage,
            maxlength: maxlengthMessage+150
        },
        /*materno: {
            required: requiredMessage,
            maxlength: maxlengthMessage+150
        },
        nacimiento: {
            required: requiredMessage,
            maxlength: maxlengthMessage+10
        },*/
        correo: {
            required: requiredMessage,
            maxlength: maxlengthMessage+150,
            email: "Ingrese un correo valido!"
        },
        /*telefono: {
            required: requiredMessage,
            maxlength: maxlengthMessage+9
        },*/
        regUsername: {
            required: requiredMessage,
            maxlength: maxlengthMessage+100
        },
        regPassword: {
            required: requiredMessage,
            maxlength: maxlengthMessage+100
        },
        regRePassword: {
            required: requiredMessage,
            maxlength: maxlengthMessage+100,
            equalTo: "Las contraseñas no coinciden!"
        }
    },
    submitHandler: function(form){
        agregar_usuario()
    },
})

//limpiar formulario registro
function limpiarFormRegistro(){
    $("#rut").val("")
    $("#nombre").val("")
    $("#paterno").val("")
    $("#materno").val("")
    $("#nacimiento").val("")
    $("#correo").val("")
    $("#telefono").val("")
    $("#regUsername").val("")
    $("#regPassword").val("")
    $("#regRePassword").val("")
}

//limpiar formulario al cerrar modal registro
$('#ba-modal-registro').on('hidden.bs.modal', function (e) {
    validarFormRegistro.resetForm()
    limpiarFormRegistro()
})

//validar formulario de tecnico
var validarFormTecnico = $("#ba-form-tecnico").submit(function(e){
    e.preventDefault()
}).validate({
    rules: {
        /*rut: {
            required: true,
            maxlength: 10
        },*/
        nombre: {
            required: true,
            maxlength: 150,
        },
        paterno: {
            required: true,
            maxlength: 150,
        },
        /*materno: {
            required: true,
            maxlength: 150,
        },
        nacimiento: {
            required: true,
            maxlength: 10,
        },*/
        correo: {
            required: true,
            maxlength: 150,
            email: true
        },
        /*telefono: {
            required: true,
            maxlength: 9,
        },*/
        regUsername: {
            required: true,
            maxlength: 100,
        },
        regPassword: {
            required: true,
            maxlength: 100,
        },
        regRePassword: {
            required: true,
            maxlength: 100,
            equalTo: "#regPassword"
        }
    },
    messages: {
        /*rut: {
            required: requiredMessage,
            maxlength: maxlengthMessage+10
        },*/
        nombre: {
            required: requiredMessage,
            maxlength: maxlengthMessage+150
        },
        paterno: {
            required: requiredMessage,
            maxlength: maxlengthMessage+150
        },
        /*materno: {
            required: requiredMessage,
            maxlength: maxlengthMessage+150
        },
        nacimiento: {
            required: requiredMessage,
            maxlength: maxlengthMessage+10
        },*/
        correo: {
            required: requiredMessage,
            maxlength: maxlengthMessage+150,
            email: "Ingrese un correo valido!"
        },
        /*telefono: {
            required: requiredMessage,
            maxlength: maxlengthMessage+9
        },*/
        regUsername: {
            required: requiredMessage,
            maxlength: maxlengthMessage+100
        },
        regPassword: {
            required: requiredMessage,
            maxlength: maxlengthMessage+100
        },
        regRePassword: {
            required: requiredMessage,
            maxlength: maxlengthMessage+100,
            equalTo: "Las contraseñas no coinciden!"
        }
    },
    submitHandler: function(form){
        agregar_tecnico()
    },
})

//limpiar formulario tecnico
function limpiarFormTecnico(){
    $("#rut").val("")
    $("#nombre").val("")
    $("#paterno").val("")
    $("#materno").val("")
    $("#nacimiento").val("")
    $("#correo").val("")
    $("#telefono").val("")
    $("#regUsername").val("")
    $("#regPassword").val("")
    $("#regRePassword").val("")
}

//limpiar formulario al cerrar modal tecnico
$('#ba-modal-tecnico').on('hidden.bs.modal', function (e) {
    validarFormTecnico.resetForm()
    limpiarFormTecnico()
})

//validar formulario de login
var validarFormLogin = $("#ba-form-login").submit(function(e){
   // e.preventDefault()
}).validate({
    rules: {
        username: {
            required: true,
            maxlength: 100
        },
        password: {
            required: true,
            maxlength: 100
        }
    },
    messages: {
        username: {
            required: requiredMessage,
            maxlength: maxlengthMessage+100
        },
        password: {
            required: requiredMessage,
            maxlength: maxlengthMessage+100
        }
    },
})

//limpiar formulario de login
function limpiarFormLogin(){
    $("#id_username").val("")
    $("#id_password").val("")
}

//limpiar formulario al cerrar modal login
$('#ba-modal-login').on('hidden.bs.modal', function (e) {
    validarFormLogin.resetForm()
    limpiarFormLogin()
})

//validar formulario solicitud
var validarFormSolicitud = $("#ba-form-solicitud").submit(function(e){
    e.preventDefault()
}).validate({
    rules: {
        fecha: {
            required: true,
            maxlength: 10
        },
        hora: {
            required: true,
            maxlength: 5
        },
        tipo: {
            requiredSelect: "default"
        },
        descripcion: {
            required: true,
            maxlength: 250
        }
    },
    messages: {
        fecha: {
            required: requiredMessage,
            maxlength: maxlengthMessage+10
        },
        hora: {
            required: requiredMessage,
            maxlength: maxlengthMessage+5
        },
        tipo: {
            requiredSelect: "Seleccione un tipo de solicitud!"
        },
        descripcion: {
            required: requiredMessage,
            maxlength: maxlengthMessage+250
        }
    },
    submitHandler: function(form){
        agregar_solicitud()
    },
})

//limpiar formulario solicitud
function limpiarFormSolicitud(){
    $("#fecha").val("")
    $("#hora").val()
    $("#tipo").val("default")
    $("#descripcion").val("")
}

//limpiar formulario solicitud y resetear
function resetFormSolicitud(){
    validarFormSolicitud.resetForm()
    limpiarFormSolicitud()
}

//validar formulario confirmar compra
var validarFormConfirmar = $("#ba-form-confirmar").submit(function(e){
    e.preventDefault()
}).validate({
    rules: {
        direccion: {
            required: true,
            maxlength: 300
        }
    },
    messages: {
        direccion: {
            required: requiredMessage,
            maxlength: maxlengthMessage+300
        }
    },
    submitHandler: function(form){
        $("#ba-modal-compra").modal('show')
        anwo_cargar_producto()
    },
})

function limpiarFormConfirmar(){
    $("#auxDescripcion").val("")
    $("#direccion").val("")
}

var validarFormCompra = $("#ba-form-compra").submit(function(e){
    e.preventDefault()
}).validate({
    submitHandler: function(form){
        agregar_user_producto()
        agregar_producto_despacho()
        anwo_actualizar_producto()
    },
})

$("#ba-modal-compra").on('hidden.bs.modal', function(e){
    validarFormConfirmar.resetForm()
    limpiarFormConfirmar()
})