function button_pago(id){
    var b = []

    //1
    var b1 = ''
    b1 += '<a target="_blank" href="https://app.payku.cl/p?a=1663p64c0" >'
    b1 += '<img  width="120px" height="auto" src="https://storage.googleapis.com/storage-payku-prd/public/img/botonpago/boton_pago_logo.png">'
    b1 += '</a>'
    b[0] = b1
    //2
    var b2 = ''
    b2 += '<a target="_blank" href="https://app.payku.cl/p?a=1663p06ee" >'
    b2 += '<img  width="120px" height="auto" src="https://storage.googleapis.com/storage-payku-prd/public/img/botonpago/boton_pago_logo.png">'
    b2 += '</a>'
    b[1] = b2
    //3
    var b3 = ''
    b3 += '<a target="_blank" href="https://app.payku.cl/p?a=1663p40e6" >'
    b3 += '<img  width="120px" height="auto" src="https://storage.googleapis.com/storage-payku-prd/public/img/botonpago/boton_pago_logo.png">'
    b3 += '</a>'
    b[2] = b3
    //4
    var b4 = ''
    b4 += '<a target="_blank" href="https://app.payku.cl/p?a=1663p126c" >'
    b4 += '<img  width="120px" height="auto" src="https://storage.googleapis.com/storage-payku-prd/public/img/botonpago/boton_pago_logo.png">'
    b4 += '</a>'
    b[3] = b4
    //5
    var b5 = ''
    b5 += '<a target="_blank" href="https://app.payku.cl/p?a=1663pdaaf" >'
    b5 += '<img  width="120px" height="auto" src="https://storage.googleapis.com/storage-payku-prd/public/img/botonpago/boton_pago_logo.png">'
    b5 += '</a>'
    b[4] = b5
    //6
    var b6 = ''
    b6 += '<a target="_blank" href="https://app.payku.cl/p?a=1663pd40e" >'
    b6 += '<img  width="120px" height="auto" src="https://storage.googleapis.com/storage-payku-prd/public/img/botonpago/boton_pago_logo.png">'
    b6 += '</a>'
    b[5] = b6
    //7
    var b7 = ''
    b7 += '<a target="_blank" href="https://app.payku.cl/p?a=1663p84bf" >'
    b7 += '<img  width="120px" height="auto" src="https://storage.googleapis.com/storage-payku-prd/public/img/botonpago/boton_pago_logo.png">'
    b7 += '</a>'
    b[6] = b7
    //8
    var b8 = ''
    b8 += '<a target="_blank" href="https://app.payku.cl/p?a=1663pe948" >'
    b8 += '<img  width="120px" height="auto" src="https://storage.googleapis.com/storage-payku-prd/public/img/botonpago/boton_pago_logo.png">'
    b8 += '</a>'
    b[7] = b8
    //9
    var b9 = ''
    b9 += '<a target="_blank" href="https://app.payku.cl/p?a=1663pef37" >'
    b9 += '<img  width="120px" height="auto" src="https://storage.googleapis.com/storage-payku-prd/public/img/botonpago/boton_pago_logo.png">'
    b9 += '</a>'
    b[8] = b9

    return b[id - 1]
}

//csrf_token para ajax
function getCookie(name){
    var cookieValue = null;
    if (document.cookie && document.cookie != ''){
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++){
            var cookie = jQuery.trim(cookies[i]);

            if (cookie.substring(0, name.length + 1) == (name + '=')){
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$.ajaxSetup({ 
    beforeSend: function(xhr, settings){
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))){
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    } 
});

//obtener fecha actual
function fecha_actual(){
    var today = new Date()
    var dd = String(today.getDate()).padStart(2, '0')
    var mm = String(today.getMonth()+1).padStart(2, '0')
    var yyyy = today.getFullYear()
    today = dd+'/'+mm+'/'+yyyy;

    return today
}

//obtener hora actual
function hora_actual(){
    var hora = new Date()
    var hh = String(hora.getHours())
    var mm = String(hora.getMinutes())
    hora = hh+':'+mm

    return hora
}

//obtener token de anwo
var token = ""
var ba_url = "http://localhost:8000/"
var anwo_url = "http://localhost:9000/"
var client_id = "WBUaoWSZt4HotRUVRDSvdkjDWCeOOvp2ElUvpAdN"
var client_secret = "MAcIbnN3iZ2ECfCUxpgP5nTBgqaakDptEg58N1xA3U3EJsZytiOiKUocLegpAQR3fMQxZrzPXIrxnobs35eEKNqH5hdCoOaEdbcHiq1tHQjw7D39EMNTmhfPKhclD2Wh"

var data = {
    grant_type: 'client_credentials',
    client_id: client_id,
    client_secret: client_secret
}

function anwo_token(){
    $.ajax({
        dataType: 'json',
        data: data,
        url: anwo_url+'oauth/token/',
        type: 'post',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        async: false,
        success: function(oauth2_token){
            token = oauth2_token.access_token
            //console.log(oauth2_token.access_token)
        },
        error: function(e){
            console.log(e)
        }
    })
}

//variables globales
var hulla = new hullabaloo();
s = "'" //comilla simple

//agregar usuario
function agregar_usuario(){
    //obtener datos
    var first_name = $("#nombre").val()
    var last_name = $("#paterno").val()
    var email = $("#correo").val()
    var username = $("#regUsername").val()
    var password = $("#regPassword").val()

    //crear form data
    var data = new FormData()
    data.append('first_name', first_name)
    data.append('last_name', last_name)
    data.append('email', email)
    data.append('username', username)
    data.append('password', password)

    //enviar datos
    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: data,
        url: ba_url+'api/user/',
        type: 'post',
        contentType: false,
        processData: false,
        cache: false,
        success: function(e){
            $("#ba-modal-registro").modal('toggle')
            $("#ba-modal-registro").on('hidden.bs.modal', function(e){
                window.hulla.send("Registro correcto!", "success");
            })
        },
        error: function(e){
            window.hulla.send("Registro incorrecto", "danger");
        }
    })
}

//agregar usuario
function agregar_tecnico(){
    //obtener datos
    var first_name = $("#nombre").val()
    var last_name = $("#paterno").val()
    var email = $("#correo").val()
    var username = $("#regUsername").val()
    var password = $("#regPassword").val()

    //crear form data
    var data = new FormData()
    data.append('first_name', first_name)
    data.append('last_name', last_name)
    data.append('email', email)
    data.append('username', username)
    data.append('password', password)

    //enviar datos
    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: data,
        url: ba_url+'api/user_staff/',
        type: 'post',
        contentType: false,
        processData: false,
        cache: false,
        success: function(e){
            $("#ba-modal-tecnico").modal('toggle')
            $("#ba-modal-tecnico").on('hidden.bs.modal', function(e){
                window.hulla.send("Registro correcto!", "success");
            })
        },
        error: function(e){
            console.log(e)
            window.hulla.send("Registro incorrecto", "danger");
        }
    })
}

//formatear precio
var fn = {
	is_number: function(value){
    	var RegExPattern = /[0-9]+$/
		return RegExPattern.test(value)
	},
	
	format_number: function(value){
    	if (fn.is_number(value)){  
			var returns = ''
			value = value.toString().split('').reverse().join('')
			var i = value.length
			while(i > 0) returns += ((i % 3 === 0 && i != value.length) ? '.' : '') + value.substring(i--, i)
			return returns
    	}
    	return 0
  	}
}

//formatear fecha
function formato_fecha(fecha, separador, nuevo){
    var arrayFecha = fecha.split(separador)
    x = arrayFecha.length-1
    fecha = ""

    while (x >= 0){
        x > 0 ? fecha += arrayFecha[x]+nuevo : fecha += arrayFecha[x]
        x--
    }

    return fecha
}

//mostrar productos de anwo
function anwo_p_process_marca(){
    $("#marca-"+arguments[0]).text(arguments[1].descripcion)
}

function anwo_productos(){
    anwo_token()

    $.ajax({
        headers: {
            'Authorization': 'Bearer '+token,
        },
        url: anwo_url+'api/producto/',
        data: null,
        type: 'get',
        success: function(productos){
            productos.forEach(producto => {
                if (producto.cantidad > 0){
                    //card por producto
                    var html = ''
                    html += '<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 d-flex justify-content-center">'
                    html += '<div class="card ba-card" style="width: 26rem;" data-aos="zoom-in">'
                    html += '<img class="ba-img-card" src="'+producto.imagesrc+'" class="card-img-top" alt="...">'
                    html += '<div class="card-body">'
                    html += '<h5 class="card-title">'+producto.descripcion+'</h5>'
                    html += '<p class="card-text">$'+fn.format_number(producto.precio)+'</p>'
                    html += '<button class="btn btn-success btn-block" data-toggle="modal" data-target="#ba-modal-producto-'+producto.id+'">Ver detalles/Comprar</button>'
                    html += '</div>'
                    html += '</div>'
                    html += '</div>'
                    $("#anwo-productos").append(html)

                    //modal por producto
                    html = ''
                    html += '<div class="modal fade" id="ba-modal-producto-'+producto.id+'" tabindex="-1" role="dialog" aria-labelledby="ba-modal-producto-'+producto.id+'" aria-hidden="true">'
                    html += '<div class="modal-dialog modal-dialog-centered" role="document">'
                    html += '<div class="modal-content">'
                    html += '<div class="modal-header">'
                    html += '<h5 class="modal-title" id="">'+producto.descripcion+'</h5>'
                    html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'
                    html += '<span aria-hidden="true">&times;</span>'
                    html += '</button>'
                    html += '</div>'
                    html += '<div class="modal-body">'
                    html += '<div class="row">'
                    html += '<div class="col-md-6 text-center">'
                    html += '<img style="height: 200px; border: 1px solid black; margin-top: 10px; margin-bottom: 10px;" src="'+producto.imagesrc+'" alt="error-foto">'
                    html += '</div>'
                    html += '<div class="col-md-6 d-flex align-items-center">'
                    html += '<table class="ba-table-anwo-producto">'
                    html += '<tr>'
                    html += '<td><b>Modelo: </b></td>'
                    html += '<td>'+producto.modelo+'</td>'
                    html += '</tr>'
                    html += '<tr>'

                    var tipo
                    switch (producto.tipo){
                        case "S":
                            tipo = "Split"
                            break
                        case "P":
                            tipo = "Portatil"
                            break
                        default:
                            window.hulla.send("[Producto]Tipo de producto invalido", "danger");
                            break
                    }

                    html += '<td><b>Tipo: </b></td>'
                    html += '<td>'+tipo+'</td>'
                    html += '</tr>'
                    html += '<tr>'
                    html += '<td><b>Marca: </b></td>'
                    html += '<td id="marca-'+producto.id+'"></td>'
                    html += '</tr>'

                    function buscar_marca(){
                        var anwo_p_id = arguments[0]

                        $.ajax({
                            headers: {
                                'Authorization': 'Bearer '+token,
                            },
                            url: arguments[1],
                            data: null,
                            type: 'get',
                            success: function(marca){
                                anwo_p_process_marca(anwo_p_id, marca)
                            },
                            error: function(e){
                                window.hulla.send("[Marca] Error al buscar marca", "danger");
                            }
                        })
                    }

                    html += '<tr>'
                    html += '<td><b>Cantidad: </b></td>'
                    html += '<td>'+producto.cantidad+'</td>'
                    html += '</tr>'
                    html += '<tr>'
                    html += '<td style="color: red !important;" colspan="2"><b>$'+fn.format_number(producto.precio)+'</b></td>'
                    html += '</tr>'
                    html += '<tr>'
                    html += '<td colspan="2">'
                    html += button_pago(producto.id)
                    html += '</td>'
                    html += '</tr>'
                    html += '</table>'
                    html += '</div>'
                    html += '</div>'
                    html += '</div>'
                    html += '<div class="modal-footer">'
                    html += '<div class="container-fluid">'
                    html += '<div class="row">'
                    html += '<div class="col-md-6">'
                    html += '<a class="btn btn-success btn-block" type="button" href="/productos/?url_producto='+producto.url+'&producto='+producto.descripcion+'">Comprar</a>'
                    html += '</div>'
                    html += '<div class="col-md-6">'
                    html += '<button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Cerrar</button>'
                    html += '</div>'
                    html += '</div>'
                    html += '</div>'
                    html += '</div>'
                    html += '</div>'
                    html += '</div>'
                    html += '</div>'
                    $("#anwo-productos-modal").append(html)
                    buscar_marca(producto.id, producto.marca)
                }
            })
            AOS.init()
        },
        error: function(e){
            //console.log(e)
            window.hulla.send("[Producto] Error al cargar productos", "danger");
        }
    })
}

function anwo_cargar_producto(){
    anwo_token()

    //obtener datos
    var url = $("#producto").val()

    $.ajax({
        headers: {
            'Authorization': 'Bearer '+token,
        },
        url: url,
        data: null,
        type: 'get',
        success: function(producto){
            $("#imagesrc").val(producto.imagesrc)
            $("#imgFoto").prop('src', producto.imagesrc)
            $("#descripcion").val(producto.descripcion)
            $("#modelo").val(producto.modelo)
            $("#txtModelo").text(producto.modelo)
            $("#precio").val(producto.precio)
            $("#txtPrecio").text('$'+fn.format_number(producto.precio))
            $("#cantidad").val(producto.cantidad)
            $("#txtCantidad").text(producto.cantidad)
            $("#tipo").val(producto.tipo)
            $("#txtTipo").text(producto.tipo)
            $("#marca").val(producto.marca)

            function buscar_marca(){
                var anwo_p_id = arguments[0]

                $.ajax({
                    headers: {
                        'Authorization': 'Bearer '+token,
                    },
                    url: arguments[1],
                    data: null,
                    type: 'get',
                    success: function(marca){
                        anwo_p_process_marca(anwo_p_id, marca)
                    },
                    error: function(e){
                        window.hulla.send("[Marca] Error al buscar marca", "danger");
                    }
                })
            }

            buscar_marca("txtMarca", producto.marca)
            $("#proveedor").val(producto.proveedor)
            $("#pagoButton").html(button_pago(producto.id))
        },
        error: function(e){
            //console.log(e)
            window.hulla.send("[Producto] Error al cargar productos", "danger");
        }
    })
}

//mostrar solicitudes
var solicitudesHeight = 0

function ba_s_process_user(){
    $("#solicitud-user-"+arguments[0]).text(arguments[1].username)
    $("#solicitud-user-email-"+arguments[0]).val(arguments[1].email)
}

function solicitudes(reload = false){
    //ajustar tamaño de contenedor de solicitudes
    if (solicitudesHeight > 0){
        $("#solicitudes").attr('style', 'min-height: '+solicitudesHeight+'px;')
    }

    //recargar?
    if (reload) {
        $("#solicitudes").html('')
    }

    contador = 0

    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        url: ba_url+'api/solicitud/',
        data: null,
        type: 'get',
        success: function(solicitudes){
            solicitudes.forEach(solicitud => {
                html = ''

                var dataAOS
                if (contador % 2 == 1){
                    dataAOS = "fade-left"
                } else{
                    dataAOS = "fade-right"
                }

                html += '<div class="col-md-12" data-aos="'+dataAOS+'">'
                html += '<div class="media mb-3 shadow p-3 ba-media-container">'
                html += '<img class="align-self-center mr-3" style="height: 64px;" src="/static/img/ba-solicitud-'+solicitud.tipo+'.png" alt="error-foto">'
                html += '<div class="media-body">'
                html += '<div class="d-flex justify-content-between">'

                var tipo
                switch (solicitud.tipo){
                    case "M":
                        tipo = "Mantención"
                        break
                    case "R":
                        tipo = "Reparación"
                        break
                    default:
                        break
                }

                html += '<h5 class="mt-0">'+tipo+'</h5>'

                var estado
                var stylestado
                switch (solicitud.estado){
                    case "C":
                        estado = "Confirmado"
                        stylestado = "text-success"
                        break
                    case "P":
                        estado = "Por confimar"
                        stylestado = "text-danger"
                        break
                    default:
                        break
                }

                if (solicitud.estado == "P"){
                    html += '<span class="align-middle '+stylestado+'">'
                    html += estado
                    html += '<input class="align-middle ml-2 ba-solicitud-radios" type="radio" name="ba-solicitud-radios" id="solicitud-radio-'+solicitud.id+'"'
                    html += 'onclick="show_mail_confirm('+s+solicitud.id+s+', '+s+solicitud.fecha+s+', '+s+solicitud.hora+s+', '+s+solicitud.descripcion+s+', '+s+solicitud.tipo+s+', '+s+solicitud.estado+s+', '+s+solicitud.user+s+')">'
                    html += '</span>'
                } else{
                    html += '<span class="align-middle '+stylestado+'">'+estado+'</span>'
                }
                html += '</div>'
                html += '<div class="mb-2">'

                function buscar_user(){
                    var ba_s_id = arguments[0]

                    $.ajax({
                        headers: {
                            'X-CSRFToken': getCookie('csrftoken')
                        },
                        url: arguments[1],
                        data: null,
                        type: 'get',
                        success: function(user){
                            ba_s_process_user(ba_s_id, user)
                        },
                        error: function(e){
                            window.hulla.send("[User] Error al buscar usuario por solicitud", "danger");
                        }
                    })
                }

                html += '<span class="mr-2 text-primary" id="solicitud-user-'+solicitud.id+'"></span>'
                html += '<input type="hidden" name="solicitud-user-email-'+solicitud.id+'" id="solicitud-user-email-'+solicitud.id+'" value="">'
                html += '<span class="mr-2">'+solicitud.fecha+'</span>'
                html += '<span>'+solicitud.hora+'</span>'
                html += '</div>'
                html += '<div class="text-justify">'
                html += solicitud.descripcion
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                $("#solicitudes").append(html)
                $("#solicitud-radio-"+solicitud.id).hide()
                buscar_user(solicitud.id, solicitud.user)
                contador++

                //ajustar tamaño de contenedor de solicitudes
                if ($("#solicitudes").height() > solicitudesHeight){
                    solicitudesHeight = $("#solicitudes").height()
                }
            })
            AOS.init()

            if (contador == 0){
                html = ''
                html += '<div class="col-md-12">'
                html += '<div class="alert alert-primary alert-dismissible fade show" role="alert">'
                html += '<strong>No se encontraron</strong> solicitudes registradas con su usuario.'
                html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                html += '<span aria-hidden="true">&times;</span>'
                html += '</button>'
                html += '</div>'
                html += '</div>'
                $("#solicitudes").html(html)
            }
        },
        error: function(e){
            window.hulla.send("[Solicitud] Error al cargar solicitudes", "danger");
        }
    })
}

//mostrar div para enviar mail
function show_mail_confirm(){
    $("#ba-mail-confirm").prop('class', 'container fixed-bottom')
    $("#bmId").val(arguments[0])
    $("#bmFecha").val(arguments[1])
    $("#bmHora").val(arguments[2])
    $("#bmDescripcion").val(arguments[3])
    $("#bmTipo").val(arguments[4])
    $("#bmEstado").val(arguments[5])
    $("#bmUser").val(arguments[6])
}

//ocultar div para enviar mail
function hide_mail_confirm(){
    $("#ba-mail-confirm").prop('class', 'd-none')
    $("input:radio[name='ba-solicitud-radios']").each(function(e) {
        this.checked = false;
    })
}

function confirmar_fecha_hora(){
    //obtener datos
    var id = $("#bmId").val()
    var fecha = $("#bmFecha").val()
    var hora = $("#bmHora").val()
    var descripcion = $("#bmDescripcion").val()
    var tipo = $("#bmTipo").val()
    var estado = $("#bmEstado").val()
    if (estado == "P"){
        estado = "C"
    }
    var user = $("#bmUser").val()

    //email
    var subject = "Buenos Aires - Confirmación de solicitud"
    var message = "[Buenos Aires]La solicitud N°"+id+" fue confirmada para la fecha "+fecha+" y hora "+hora
    var recipient = $("#solicitud-user-email-"+id).val()

    //crear form data
    var data = new FormData()
    data.append('fecha', fecha)
    data.append('hora', hora)
    data.append('descripcion', descripcion)
    data.append('tipo', tipo)
    data.append('estado', estado)
    data.append('user', user)

    //enviar datos
    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: data,
        url: ba_url+'api/solicitud/'+id+'/',
        type: 'PUT',
        contentType: false,
        processData: false,
        cache: false,
        success: function(result){
            window.hulla.send("Fecha de solicitud confirmada!", "success");
            solicitudes(true)
            ba_mail(subject, message, recipient)
            hide_mail_confirm()
        },
        error: function(e){
            console.log(e)
            window.hulla.send("[Solicitud] Error al confirmar fecha de solicitud", "danger");
        }
    })
}

//enviar email
function ba_mail(){
    //obtener datos
    var subject = arguments[0]
    var message = arguments[1]
    var recipient = arguments[2]

    //enviar datos
    var data = new FormData()
    data.append('subject', subject)
    data.append('message', message)
    data.append('recipient', recipient)

    //enviar datos
    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: data,
        url: '/ba_mail/',
        type: 'post',
        contentType: false,
        processData: false,
        cache: false,
        success: function(result){
            window.hulla.send(result, "success");
        },
        error: function(result){
            window.hulla.send(result, "danger");
        }
    })
}

//agregar nueva solicitud
function agregar_solicitud(){
    //obtener datos
    var fecha = formato_fecha($("#fecha").val(), "-", "/")
    var hora = $("#hora").val()
    var descripcion = $("#descripcion").val()
    var tipo = $("#tipo").val()
    var user = $("#user").val()
    user = ba_url+'api/user/'+user+'/'

    //crear form data
    var data = new FormData()
    data.append('fecha', fecha)
    data.append('hora', hora)
    data.append('descripcion', descripcion)
    data.append('tipo', tipo)
    data.append('user', user)

    //enviar datos
    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: data,
        url: ba_url+'api/solicitud/',
        type: 'post',
        contentType: false,
        processData: false,
        cache: false,
        success: function(result){
            window.hulla.send("Solicitud creada!", "success");
            solicitudes(true)
        },
        error: function(e){
            window.hulla.send("[Solicitud] Error al crear solicitud", "danger");
        }
    })
}

//agregar producto a usuario
function agregar_user_producto(){
    //obtener datos
    var user = $("#user").val()
    user = ba_url+'api/user/'+user+'/'
    var producto = $("#producto").val()

    //crear form data
    var data = new FormData()
    data.append('user', user)
    data.append('producto', producto)

    //enviar datos
    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: data,
        url: ba_url+'api/user_producto/',
        type: 'post',
        contentType: false,
        processData: false,
        cache: false,
        success: function(result){
            window.hulla.send("Producto comprado!", "success");
        },
        error: function(e){
            window.hulla.send("[Producto] Error al comprar producto", "danger");
        }
    })
}

//agregar producto a despacho
function agregar_producto_despacho(){
    //obtener datos
    var user = $("#user").val()
    user = ba_url+'api/user/'+user+'/'
    var producto = $("#producto").val()
    var direccion = $("#direccion").val()
    var fecha = fecha_actual()
    var hora = hora_actual()

    //crear form data
    var data = new FormData()
    data.append('user', user)
    data.append('producto', producto)
    data.append('direccion', direccion)
    data.append('fecha_pedido', fecha)
    data.append('hora_pedido', hora)

    //enviar datos
    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        data: data,
        url: ba_url+'api/producto_despacho/',
        type: 'post',
        contentType: false,
        processData: false,
        cache: false,
        success: function(result){
            $("#ba-modal-compra").modal('toggle')
            $("#ba-modal-compra").on('hidden.bs.modal', function(e){
                window.hulla.send("Producto a despacho!", "success");
                cargar_user_productos(true)
            })
        },
        error: function(e){
            window.hulla.send("[Producto] Error al enviar producto a despacho", "danger");
        }
    })
}

//actualizar stock producto ANWO
function anwo_actualizar_producto(){
    anwo_token()

    //obtener datos
    var url = $("#producto").val()
    var imagesrc = $("#imagesrc").val()
    var descripcion = $("#descripcion").val()
    var modelo = $("#modelo").val()
    var precio = $("#precio").val()
    var cantidad = $("#cantidad").val()
    cantidad = cantidad - 1
    var tipo = $("#tipo").val()
    var marca = $("#marca").val()
    var proveedor = $("#proveedor").val()

    //crear form data
    var data = new FormData()
    data.append('imagesrc', imagesrc)
    data.append('descripcion', descripcion)
    data.append('modelo', modelo)
    data.append('precio', precio)
    data.append('cantidad', cantidad)
    data.append('tipo', tipo)
    data.append('marca', marca)
    data.append('proveedor', proveedor)

    //enviar datos
    $.ajax({
        headers: {
            'Authorization': 'Bearer '+token,
        },
        url: url,
        data: data,
        type: 'PUT',
        contentType: false,
        processData: false,
        cache: false,
        success: function(e){
            window.hulla.send("Stock ANWO actualizado!", "success");
        },
        error: function(e){
            //console.log(e)
            window.hulla.send("[Producto] Error actualizar stock", "danger");
        }
    })
}

//buscar estado por producto
function buscar_estado_producto(){
    var user = $("#user").val()
    user = ba_url+'api/user/'+user+'/'

    var p_id = arguments[0]
    var producto = arguments[1]

    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        url: ba_url+'api/producto_despacho/',
        data: null,
        type: 'get',
        success: function(despachos){
            despachos.forEach(despacho => {
                if (despacho.user == user && despacho.producto == producto){
                    var estado
                    if (despacho.estado == "B"){
                        estado = "Bodega"
                        $("#txtEst-"+p_id).text(estado)
                        $("#txtFp-"+p_id).text(despacho.fecha_pedido)
                        $("#txtHp-"+p_id).text(despacho.hora_pedido)
                    } else if (despacho.estado == "D"){
                        estado = "Despacho"
                        $("#txtEst-"+p_id).text(estado)
                        $("#titFp").text('Fecha despacho:')
                        $("#txtFp-"+p_id).text(despacho.fecha_despacho)
                        $("#titFp").text('Hora despacho')
                        $("#txtHp-"+p_id).text(despacho.hora_despacho)
                    } else{
                        estado = "Entregado"
                        $("#txtEst-"+p_id).text(estado)
                        $("#titFp").text('Fecha entrega:')
                        $("#txtFp-"+p_id).text(despacho.fecha_entrega)
                        $("#titFp").text('Hora entrega')
                        $("#txtHp-"+p_id).text(despacho.hora_entrega)
                    }
                }
            })
        },
        error: function(e){
            window.hulla.send("[Producto] Error al cargar estado", "danger");
        }
    })
}

//buscar producto por usuario
function anwo_buscar_user_producto(){
    anwo_token()

    var p_id = arguments[0]

    $.ajax({
        headers: {
            'Authorization': 'Bearer '+token,
        },
        url: arguments[1],
        data: null,
        type: 'get',
        success: function(producto){
            $("#imgFo-"+p_id).prop('src', producto.imagesrc)
            $("#txtDesc-"+p_id).text(producto.descripcion)
            $("#txtMod-"+p_id).text(producto.modelo)
            
            var tipo
            if (producto.tipo == "S"){
                tipo = "Split"
            } else{
                tipo = "Portatil"
            }
            $("#txtTip-"+p_id).text(tipo)

            $("#txtPre-"+p_id).text('$'+fn.format_number(producto.precio))

            function buscar_marca(){
                var anwo_p_id = arguments[0]

                $.ajax({
                    headers: {
                        'Authorization': 'Bearer '+token,
                    },
                    url: arguments[1],
                    data: null,
                    type: 'get',
                    success: function(marca){
                        anwo_p_process_marca(anwo_p_id, marca)
                    },
                    error: function(e){
                        window.hulla.send("[Marca] Error al buscar marca", "danger");
                    }
                })
            }

            buscar_marca("txtMar-"+p_id, producto.marca)
            buscar_estado_producto(p_id, producto.url)
        },
        error: function(e){
            //console.log(e)
            window.hulla.send("[Producto] Error al cargar producto", "danger");
        }
    })
}

//cargar productos por usuario
function cargar_user_productos(reload = false){
    //recargar?
    if (reload) {
        $("#productos").html('')
    }

    contador = 0

    $.ajax({
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        url: ba_url+'api/user_producto/',
        data: null,
        type: 'get',
        success: function(productos){
            productos.forEach(producto => {
                html = ''
                html += '<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 d-flex justify-content-center">'
                html += '<div class="card ba-card" style="width: 26rem;" data-aos="zoom-in">'
                html += '<img class="card-img-top ba-img-card" src="" alt="error-foto" id="imgFo-'+producto.id+'">'
                html += '<div class="card-body">'
                html += '<h5 id="txtDesc-'+producto.id+'"></h5>'
                html += '<table>'
                html += '<tr>'
                html += '<td><b>Modelo:</b></td>'
                html += '<td><span id="txtMod-'+producto.id+'"></span></td>'
                html += '</tr>'
                html += '<tr>'
                html += '<td><b>Tipo:</b></td>'
                html += '<td><span id="txtTip-'+producto.id+'"></span></td>'
                html += '</tr>'
                html += '<tr>'
                html += '<td><b>Marca:</b></td>'
                html += '<td><span id="marca-txtMar-'+producto.id+'"></span></td>'
                html += '</tr>'
                html += '<tr>'
                html += '<td><b>Precio:</b></td>'
                html += '<td><b><span id="txtPre-'+producto.id+'"></span></b></td>'
                html += '</tr>'
                html += '<tr>'
                html += '<td colspan="2"><hr></td>'
                html += '</tr>'
                html += '<tr>'
                html += '<td><b>Estado:</b></td>'
                html += '<td><span id="txtEst-'+producto.id+'"></span></td>'
                html += '</tr>'
                html += '<tr>'
                html += '<td><b><span id="titFp">Fecha pedido:</span></b></td>'
                html += '<td><span id="txtFp-'+producto.id+'"></span></td>'
                html += '</tr>'
                html += '<tr>'
                html += '<td><b><span id="titHp">Hora pedido:</span></b></td>'
                html += '<td><span id="txtHp-'+producto.id+'"></span></td>'
                html += '</tr>'
                html += '</table>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                $("#productos").append(html)
                anwo_buscar_user_producto(producto.id, producto.producto)
                contador++
            })
            AOS.init()

            if (contador == 0){
                html = ''
                html += '<div class="col-md-12">'
                html += '<div class="alert alert-primary alert-dismissible fade show" role="alert">'
                html += '<strong>No se encontraron</strong> productos registrados con su usuario.'
                html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                html += '<span aria-hidden="true">&times;</span>'
                html += '</button>'
                html += '</div>'
                html += '</div>'
                $("#productos").html(html)
            }
        },
        error: function(e){
            window.hulla.send("[Producto] Error al cargar productos", "danger");
        }
    })
}