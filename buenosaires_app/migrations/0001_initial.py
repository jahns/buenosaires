# Generated by Django 2.2.2 on 2019-06-20 04:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Solicitud',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.CharField(max_length=10)),
                ('descripcion', models.CharField(max_length=250)),
                ('tipo', models.CharField(choices=[('R', 'Reparación'), ('M', 'Mantención')], max_length=50)),
                ('estado', models.CharField(choices=[('C', 'Confirmado'), ('P', 'Por confirmar')], max_length=50)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
