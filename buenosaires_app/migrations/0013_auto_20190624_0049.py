# Generated by Django 2.2.2 on 2019-06-24 04:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('buenosaires_app', '0012_auto_20190624_0048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productodespacho',
            name='hora_despacho',
            field=models.CharField(default='00:00', max_length=5, null=True),
        ),
        migrations.AlterField(
            model_name='productodespacho',
            name='hora_entrega',
            field=models.CharField(default='00:00', max_length=5, null=True),
        ),
        migrations.AlterField(
            model_name='productodespacho',
            name='hora_pedido',
            field=models.CharField(default='00:00', max_length=5, null=True),
        ),
    ]
