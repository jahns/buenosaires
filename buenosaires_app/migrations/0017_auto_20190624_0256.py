# Generated by Django 2.2.2 on 2019-06-24 06:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('buenosaires_app', '0016_auto_20190624_0256'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solicitud',
            name='tipo',
            field=models.CharField(choices=[('R', 'Reparación'), ('M', 'Mantención')], max_length=50),
        ),
    ]
