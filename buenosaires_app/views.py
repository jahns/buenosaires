from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.core.mail import send_mail
from .models import Solicitud, UserProducto, ProductoDespacho

#rest framework
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from .serializers import UserSerializer, UserStaffSerializer, SolicitudSerializer, UserProductoSerializer, ProductoDespachoSerializer

# Create your views here.
def index(request):
    return render(
        request,
        'index.html',
        {}
    )

def solicitudes(request):
    return render(
        request,
        'solicitudes.html',
        {}
    )

def productos(request):
    return render(
        request,
        'productos.html',
        {}
    )

def ba_mail(request):
    subject = request.POST.get('subject', '')
    message = request.POST.get('message', '')
    recipient = request.POST.get('recipient', '')
    from_email = request.user.email
    result = ""

    try:
        send_mail(
            subject,
            message,
            from_email,
            [recipient],
            fail_silently = False,
        )
        result = "Correo enviado!"
    except smptlib.SMTPException:
        result = "Error al enviar correo"

    return HttpResponse(result)

# Create your rest views here.
class UserView(viewsets.ModelViewSet):
    #queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        user = self.request.user
        
        if user.is_superuser or user.is_staff:
            queryset = User.objects.all()
        else:
            queryset = User.objects.filter(id = user.id)

        return queryset

class UserStaffView(viewsets.ModelViewSet):
    #queryset = User.objects.all()
    serializer_class = UserStaffSerializer
    authentication_classes = (
        TokenAuthentication,
        SessionAuthentication,
    )
    permission_classes = (
        IsAuthenticated,
        IsAdminUser,
    )

    def get_queryset(self):
        user = self.request.user
        
        if user.is_superuser or user.is_staff:
            queryset = User.objects.filter(is_staff = True)
        else:
            queryset = User.objects.filter(id = user.id)

        return queryset

class SolicitudView(viewsets.ModelViewSet):
    #queryset = Solicitud.objects.all()
    serializer_class = SolicitudSerializer
    authentication_classes = (
        TokenAuthentication,
        SessionAuthentication,
    )
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user

        if user.is_superuser or user.is_staff:
            queryset = Solicitud.objects.all()
        else:
            queryset = Solicitud.objects.filter(user = user.id)

        return queryset

class UserProductoView(viewsets.ModelViewSet):
    #queryset = UserProducto.objects.all()
    serializer_class = UserProductoSerializer
    authentication_classes = (
        TokenAuthentication,
        SessionAuthentication,
    )
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user

        if user.is_superuser or user.is_staff:
            queryset = UserProducto.objects.all()
        else:
            queryset = UserProducto.objects.filter(user = user.id)

        return queryset

class ProductoDespachoView(viewsets.ModelViewSet):
    #queryset = ProductoDespacho.objects.all()
    serializer_class = ProductoDespachoSerializer
    authentication_classes = (
        TokenAuthentication,
        SessionAuthentication,
    )
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user

        if user.is_superuser or user.is_staff:
            queryset = ProductoDespacho.objects.all()
        else:
            queryset = ProductoDespacho.objects.filter(user = user.id)

        return queryset