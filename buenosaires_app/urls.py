from django.urls import path, include, reverse_lazy
from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView, LogoutView

#django rest framework
from rest_framework import routers
from rest_framework.authtoken import views as rest_views

router = routers.DefaultRouter()
router.register(r'user', views.UserView, basename = 'user')
router.register(r'user_staff', views.UserStaffView, basename = 'user_staff')
router.register(r'solicitud', views.SolicitudView, basename = 'solicitud')
router.register(r'user_producto', views.UserProductoView, basename = 'userproducto')
router.register(r'producto_despacho', views.ProductoDespachoView, basename = 'productodespacho')

urlpatterns = [
    path('', views.index, name='index'),
    path('solicitudes/', views.solicitudes, name='solicitudes'),
    path('productos/', views.productos, name='productos'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('ba_mail/', views.ba_mail, name='ba_mail'),
    #rest api
    path('api/', include(router.urls)),
    path('api-token-auth/', rest_views.obtain_auth_token, name='api-token-auth')
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
